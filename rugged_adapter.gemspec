# frozen_string_literal: true

$:.push File.expand_path('../lib', __FILE__)
require 'rugged_adapter/version'

Gem::Specification.new do |s|
  s.name        = 'gitlab-gollum-rugged_adapter'
  s.version     = Gollum::Lib::Git::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ['Bart Kamphorst, Dawa Ometto']
  s.email       = ['fjlopez@gitlab.com']
  s.homepage    = 'https://gitlab.com/gitlab-org/gitlab-gollum-rugged_adapter'
  s.summary     = %q{Adapter for Gollum to use Rugged (libgit2) at the backend.}
  s.description = %q{Adapter for Gollum to use Rugged (libgit2) at the backend.}
  s.license	= 'MIT'

  s.add_runtime_dependency 'rugged', '~> 1.0'
  s.add_runtime_dependency 'mime-types', '>= 1.15'
  s.add_development_dependency 'rspec', '3.4.0'

  s.files = Dir[
    'lib/**/*',
    'README.md',
    'LICENSE'
  ]
  s.require_paths = ['lib']
end
